package com.example.cswexam.service;

import com.example.cswexam.entity.Product;
import com.example.cswexam.repository.ProductRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service

public class ProductService {
    private final ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product save(Product product){
        return productRepository.save(product);
    }

    public List<Product> findAll(){
        return productRepository.findAll();
    }

    public void deletedById(Integer id){
        productRepository.deleteById(id);
    }

    public Optional<Product> findById(Integer id){
        return productRepository.findById(id);
    }


}
